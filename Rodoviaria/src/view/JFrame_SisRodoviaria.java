package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

public class JFrame_SisRodoviaria extends JFrame {

	private JPanel contentPane;
	private JTextField JTextField_Usuario;
	private JPasswordField JPasswordField_Senha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFrame_SisRodoviaria frame = new JFrame_SisRodoviaria();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFrame_SisRodoviaria() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel JLabel_BemVindo = new JLabel("Bem vindo ao SisRodoviaria!");
		JLabel_BemVindo.setFont(new Font("Tahoma", Font.BOLD, 20));
		JLabel_BemVindo.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel_BemVindo.setBounds(10, 11, 414, 51);
		contentPane.add(JLabel_BemVindo);
		
		JLabel JLabel_Usuario = new JLabel("Usu\u00E1rio:");
		JLabel_Usuario.setHorizontalAlignment(SwingConstants.RIGHT);
		JLabel_Usuario.setFont(new Font("Tahoma", Font.BOLD, 16));
		JLabel_Usuario.setBounds(10, 73, 130, 40);
		contentPane.add(JLabel_Usuario);
		
		JLabel JLabel_Senha = new JLabel("Senha:");
		JLabel_Senha.setHorizontalAlignment(SwingConstants.RIGHT);
		JLabel_Senha.setFont(new Font("Tahoma", Font.BOLD, 16));
		JLabel_Senha.setBounds(10, 124, 130, 40);
		contentPane.add(JLabel_Senha);
		
		JTextField_Usuario = new JTextField();
		JTextField_Usuario.setBounds(150, 85, 188, 20);
		contentPane.add(JTextField_Usuario);
		JTextField_Usuario.setColumns(10);
		
		JPasswordField_Senha = new JPasswordField();
		JPasswordField_Senha.setBounds(150, 136, 188, 20);
		contentPane.add(JPasswordField_Senha);
		
		JButton JButton_Acessar = new JButton("Acessar");
		JButton_Acessar.setBounds(85, 175, 89, 23);
		contentPane.add(JButton_Acessar);
		
		JButton JButton_Cancelar = new JButton("Cancelar");
		JButton_Cancelar.setBounds(259, 175, 89, 23);
		contentPane.add(JButton_Cancelar);
	}
}
